<?php

namespace App\Repository;

use App\Entity\Proffesors;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Proffesors|null find($id, $lockMode = null, $lockVersion = null)
 * @method Proffesors|null findOneBy(array $criteria, array $orderBy = null)
 * @method Proffesors[]    findAll()
 * @method Proffesors[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProffesorsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Proffesors::class);
    }

    // /**
    //  * @return Proffesors[] Returns an array of Proffesors objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Proffesors
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
