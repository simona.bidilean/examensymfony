/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../css/students.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("bootstrap");
var dt = require( 'datatables.net' )();

$(document).ready( function () {
    $('#table_id').DataTable();
} );

$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": '../examen/json.data.json'
    } );
} );
